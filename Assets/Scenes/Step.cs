using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Step : MonoBehaviour
{
    AudioSource ASource;
    [SerializeField] List<AudioClip> audiosList;
    AudioClip currentStep;
    bool stepSound;

    void Start()
    {
        ASource = GetComponent<AudioSource>();
        stepSound = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (stepSound)
        {
            setNextStep();
            ASource.PlayOneShot(currentStep);
            StartCoroutine(blockStepAudio());
        }
       
    }

    IEnumerator blockStepAudio()
    {
        stepSound = false;
        yield return new WaitForSeconds(currentStep.length);
        stepSound = true;
    }

    void setNextStep()
    {
        var i = Random.Range(0, audiosList.Count - 1);
        currentStep = audiosList[i];
    }
}