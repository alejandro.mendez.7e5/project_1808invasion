using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponClass : MonoBehaviour
{
    protected Weapon _weapon;
    protected int click;
    protected PlayerAnimationController animPlayerCont;

    protected void Awake()
    {
        //Debug.Log(this.name);
        //animPlayerCont = GameObject.Find("Erika").GetComponent<PlayerAnimationController>();
    }
 
    public void SelectWeapon(Weapon newWeapon) 
    {
        Debug.Log("Nueva " + newWeapon);
        if (_weapon is null)
        {
            _weapon = newWeapon;
        }
        else {
            if (_weapon != newWeapon) {
                //Destroy(_weapon);
                _weapon = newWeapon;
                //Instantiate(_weapon.weaponModel);
                //_weapon.Instantiate(newWeapon);
            }
        }
    }
    public void Attack()
    {
        animPlayerCont = GameObject.Find("Erika").GetComponent<PlayerAnimationController>();
        click++;
        FunCheckClick();

        animPlayerCont.AnimationAttack(click);
        animPlayerCont.AnimationAttack1(click);
        animPlayerCont.AnimationAttack2(click);
        animPlayerCont.AnimationAttack3(click);
    }

    public void Aim(bool aim)
    {
        animPlayerCont = GameObject.Find("Erika").GetComponent<PlayerAnimationController>();
        animPlayerCont.Aiming(aim);
        Debug.Log("I'm aiming");
    }

    public void Shoot()
    {
        animPlayerCont = GameObject.Find("Erika").GetComponent<PlayerAnimationController>();
        animPlayerCont.AnimationShoot();
        Debug.Log("Shoot");
    }

    public virtual void FunCheckClick()
    {
        if (click > 3) click = 0;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent<IDamegeable>(out IDamegeable damegeable))
        {
            damegeable.ApplyDamage(_weapon.Damage);
        }

    }
    /*
    public virtual void Combo()
    {
        switch (click)
        {
            case 1:
                Debug.Log("Ataque 1");
                StartCoroutine(StopAnimation(0f));
                break;
            case 2:
                animPlayerCont.AnimationAttack2();
                Debug.Log("Ataque 2");
                StartCoroutine(StopAnimation(2f _weapon.TimeAnim1));
                break;
            case 3:
                animPlayerCont.AnimationAttack2();
                Debug.Log("Ataque 2.5");
                StartCoroutine(TimeAnimation(2.2f _weapon.TimeAnim2));
                break;
            default:
                StartCoroutine(StopAnimation(0f));
                break;

        }
    }*/

    /*
    IEnumerator TimeAnimation(float time)
    {
        yield return new WaitForSeconds(time);
        Debug.Log("Ataque 3");
        animPlayerCont.AnimationAttack3();
        StartCoroutine(StopAnimation(1.20f _weapon.TimeAnim3));
    }

    IEnumerator StopAnimation(float time)
    {
        yield return new WaitForSeconds(time);
        Debug.Log("Stop");
        animPlayerCont.AnimationAttack();
        click = 0;
    }*/
}
