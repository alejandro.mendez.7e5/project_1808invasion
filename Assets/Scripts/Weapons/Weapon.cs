using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Weapon_Data", menuName = "New ScriptableObject/New Weapon")]
public class Weapon : ScriptableObject
{
    private PlayerAnimationController _playerAnimationController;
    public enum atackStatus
    {
        aming, stand
    }
    public enum weaponType
    {
        ranged, meele, throweable
    }

    [SerializeField] 
    private string _name;
    [SerializeField] 
    private float _damage;
    [SerializeField]
    private float _range;
    [SerializeField]
    private atackStatus _atackStatus;
    [SerializeField]
    private weaponType _weaponType;
    [SerializeField]
    private float _coolDown;
    [SerializeField]
    private int _indexLayer;
    [SerializeField]
    private GameObject _weaponModel;
    [SerializeField]
    private GameObject _bullet;
    [SerializeField]
    private Vector3 _positon;
    [SerializeField]
    private Vector3 _rotation;

    private GameObject instancedWeapon;
    private WeaponClass weaponCurrent;
    //private int _lastIndexLayer = 1;

    public string Name { get { return _name; } }

    public float Damage { get { return _damage; } }
    
    public float Range { get { return _range; } }

    public atackStatus atackstatus { get { return _atackStatus; } }

    public weaponType Weapontype { get { return _weaponType; } }

    public float CoolDown { get { return _coolDown; } }

    //public int LastIndexLayer { get { return _lastIndexLayer; } }

    public int IndexLayer { get { return _indexLayer; } }

    public GameObject weaponModel { get { return _weaponModel; } }

    public GameObject Bullet { get { return _bullet; } }

    public void Attack()
    {
        weaponCurrent = weaponModel.GetComponent<WeaponClass>();
        weaponCurrent.Attack();
    }

    public void AimAttack(bool aim)
    {
        weaponCurrent = weaponModel.GetComponent<WeaponClass>();
        weaponCurrent.Aim(aim);
    }

    public void ShootAttack()
    {
        weaponCurrent = weaponModel.GetComponent<WeaponClass>();
        weaponCurrent.Shoot();
    }

    public void Instantiate(Weapon newWeapon)
    {
        var player = GameObject.Find("Erika").GetComponent<Animator>();
        var parentObject = GameObject.Find("mixamorig:RightHand");
        GameObject _object = Instantiate(newWeapon.weaponModel, parentObject.transform);
        _object.transform.localPosition = _positon;
        _object.transform.localRotation = Quaternion.Euler(_rotation);
        instancedWeapon = _object;
        //ChangeAnim(newWeapon);
        player.SetLayerWeight(newWeapon.IndexLayer, 1);
        instancedWeapon.GetComponent<WeaponClass>().SelectWeapon(newWeapon);
    }

    public void DestroyInstance()
    {
        var player = GameObject.Find("Erika").GetComponent<Animator>();
        player.SetLayerWeight(IndexLayer, 0);
        Destroy(instancedWeapon);
    }
}
