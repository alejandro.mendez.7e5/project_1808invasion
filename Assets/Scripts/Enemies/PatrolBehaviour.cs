using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class PatrolBehaviour : MonoBehaviour
{
    [SerializeField] Transform[] patrolRoutePoints;
    [SerializeField] int currentPoint;
    [SerializeField] GameObject patrolPoints;

    Vector3 target;
    NavMeshAgent agent;

    private void OnEnable()
    {
        agent = GetComponent<NavMeshAgent>();
        GetCloserPatrolPoint();
        UpdateDestination();
    }

    public void Update()
    {
        if (Vector3.Distance(transform.position, target) < 1.25f)
        {
            ChangeDestinationIndex();
            UpdateDestination();
        }
    }

    void UpdateDestination()
    {
        target = patrolRoutePoints[currentPoint].position;
        agent.SetDestination(target);
    }

    void ChangeDestinationIndex()
    {
        currentPoint++;
        if (currentPoint == patrolRoutePoints.Length) currentPoint = 0;
    }

    void GetCloserPatrolPoint()
    {
        //La posici�n de todos los puntos
        Transform[] allPoints;

        //Obtiene los transforms de todos los puntos
        allPoints = patrolPoints.GetComponentsInChildren<Transform>();

        //El objeto padre que contiene todos los puntos se incluye dentro de allPoints, en la primera posici�n
        //Por ello lo sobreescribo utilizando Skip para saltarme el primer elemento.
        allPoints = allPoints.Skip(1).ToArray();

        //El punto m�s cercano al enemigo
        Transform closerPoint;

        closerPoint = allPoints[0]; //Le establezco uno cualquiera para que tenga con qu� comparar

        //Por cada punto, compara la distancia entre el enimgo y este. En caso de que la distancia sea menor que la del
        //punto m�s cercano, �ste se establece como el m�s cercano

        foreach (var point in allPoints) 
            if (Vector3.Distance(transform.position, point.transform.position)
                < Vector3.Distance(transform.position, closerPoint.transform.position))
            {
                closerPoint = point;
            }

        target = closerPoint.transform.position;

        //Para que se "acople" correctamente de nuevo a la ruta tiene que saber en qu� punto se encuentre
        //para luego ir al siguiente
        for (int i = 0; i < allPoints.Length; i++)
        {
            if (allPoints[i] == closerPoint)
            {
                currentPoint = i;
                break;
            }
        }
    }
}
