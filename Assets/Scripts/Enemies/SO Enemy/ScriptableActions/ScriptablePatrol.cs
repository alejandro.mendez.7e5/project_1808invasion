using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Patrol", menuName = "ScriptableObjects/EnemyScriptableAction/ScriptablePatrol")]
public class ScriptablePatrol : ScriptableAction
{
    PatrolBehaviour patrol;

    public override void OnFinishedState()
    {
        sc.gameObject.GetComponent<PatrolBehaviour>().enabled = false;
    }

    public override void OnSetState(StateController sC)
    {
        base.OnSetState(sC);
        Debug.LogWarning("Comienzo a patrullar");

        patrol = sc.gameObject.GetComponent<PatrolBehaviour>();
        patrol.enabled = true;

        /*
        var follow = sc.gameObject.GetComponent<FollowBehaviour>();
        if (follow.enabled) follow.enabled = false;
        */
    }

    public override void OnUpdate()
    { }
}
