using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Attack", menuName = "ScriptableObjects/EnemyScriptableAction/ScriptableAttack")]
public class ScriptableAttack : ScriptableAction
{
    public override void OnFinishedState()
    {
        Debug.LogWarning("Dejo de atacar");
    }

    public override void OnSetState(StateController sC)
    {
        base.OnSetState(sC);
        Debug.LogWarning("Te reviento nano");
    }

    public override void OnUpdate()
    {
        Debug.Log("Te estoy atacando");
    }
}