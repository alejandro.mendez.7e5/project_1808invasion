using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Watch", menuName = "ScriptableObjects/EnemyScriptableAction/ScriptableWatch")]
public class ScriptableWatch : ScriptableAction
{
    public override void OnFinishedState()
    {
        Debug.LogWarning("Dejo de mirar");
    }

    public override void OnSetState(StateController sC)
    {
        base.OnSetState(sC);
        Debug.LogWarning("Comienzo a mirar");
    }

    public override void OnUpdate()
    {
        Debug.Log("Estoy mirando");
    }
}