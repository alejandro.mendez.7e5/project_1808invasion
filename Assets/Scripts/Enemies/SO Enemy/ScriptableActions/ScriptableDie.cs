using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Die", menuName = "ScriptableObjects/EnemyScriptableAction/ScriptableDie")]
public class ScriptableDie : ScriptableAction
{
    public override void OnFinishedState()
    {
        Debug.LogWarning("No puedo revivir �?");
    }

    public override void OnSetState(StateController sC)
    {
        base.OnSetState(sC);
        Debug.LogWarning("Oh, im die, thank you forever");
    }

    public override void OnUpdate()
    {
        Debug.Log("Estoy muerto");
    }
}