using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Follow", menuName = "ScriptableObjects/EnemyScriptableAction/ScriptableFollow")]
public class ScriptableFollow : ScriptableAction
{
    FollowBehaviour follow;
    EnemyStateController enemyC;

    public override void OnFinishedState()
    {
        Debug.LogWarning("Dejo de seguirte");
        follow.enabled = false;
    }

    public override void OnSetState(StateController sC)
    {
        base.OnSetState(sC);

        enemyC = (EnemyStateController) sc;
        Debug.LogWarning("Te comienzo a seguir");

        follow = sc.gameObject.GetComponent<FollowBehaviour>();
        follow.enabled = true;
    }

    public override void OnUpdate()
    {
        Debug.Log("Han pasado " + follow.forget + " segundos de los " + follow.timeUntilForget + " que tiene que pasar");

        if (follow.forget >= follow.timeUntilForget) {
            sc.StateTransition(enemyC.patrol);
            Debug.Log("Ha pasado el tiempo");
        }  
    }
}