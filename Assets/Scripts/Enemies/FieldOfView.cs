using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{
    public float visionRadius, visionAngle;
    [SerializeField] private LayerMask playerMask, obstacleMask;

    public GameObject playerToFollow;
    public bool canSeePlayer;

    //EnemyStates enemyStates;

    private void Start()
    {
        //enemyStates = GetComponent<EnemyStates>();
        playerToFollow = GameObject.Find("Player");
    }

    void Update()
    {
        FoV();
    }

    //Gizmos are used as a viual help in the editor mode. In this case, we use it to see the vision radious, so we can test better
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, visionRadius);
    }

    public void FoV()
    {
        Collider[] ObjesctsInsideTheRadius = Physics.OverlapSphere(transform.position, visionRadius, playerMask);

        if (ObjesctsInsideTheRadius.Length != 0)
        {
            /*The only thing the OverlapShpere can find is the player, as we are searching
             for the Player layerMask. So always we would want to pick the [0]*/
            Transform target = ObjesctsInsideTheRadius[0].transform;

            /*If we have found the player, we create a Vector3 with the direction towards it.
             We would use it to check if there is something between them*/
            Vector3 targetDirection = (target.position - transform.position).normalized;

            if (Vector3.Angle(transform.forward, targetDirection) < visionAngle / 2)
            {
                /*If the player is inside the radious range AND the angle range we throw
                 a ray towards the player. If it collides with something tagged "Obstacle" it means
                there is something that blocks the vision and the enemy can't see the player*/
                float distanceToTarget = Vector3.Distance(transform.position, target.position);

                if (!Physics.Raycast(transform.position, targetDirection, distanceToTarget, obstacleMask))
                {
                    canSeePlayer = true;                
                    //enemyStates.ChangeState(EnemyStates.EnemyState.Follow);
                }
                else canSeePlayer = false;
            }
            else canSeePlayer = false;
        }
        else if (canSeePlayer) canSeePlayer = false;
    }
}
