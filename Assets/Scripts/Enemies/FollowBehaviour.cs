using UnityEngine;
using UnityEngine.AI;

public class FollowBehaviour : MonoBehaviour
{
    NavMeshAgent agent;
    [SerializeField] Transform follow;
    [SerializeField] LayerMask obstacleMask, playerMask;

    [SerializeField] float newRadious, newAngle;
    public float timeUntilForget;
    public float forget;

    FieldOfView fov;

    float originalRadius, originalAngle;

    EnemyStates enemyStates;

    public void Follow()
    {
        agent.destination = follow.position;
    }

    public void Update()
    {
        Follow();
        fov.FoV();
        Forget();
    }
    
    void UpdateFovValues()
    {
        fov.visionAngle = newAngle;
        fov.visionRadius = newRadious;
    }

    void RestoreFovValues()
    {
        fov.visionAngle = originalAngle;
        fov.visionRadius = originalRadius;
    }

    private void OnEnable()
    {
        if (follow == null) follow = GameObject.Find("Player").GetComponent<Transform>();
        
        fov = GetComponent<FieldOfView>();
        agent = GetComponent<NavMeshAgent>();
        agent.speed = 5;

        UpdateFovValues();

        originalRadius = fov.visionRadius;
        originalAngle = fov.visionAngle;

        forget = 0;

        enemyStates = GetComponent<EnemyStates>();
    }

    private void OnDisable()
    {
        RestoreFovValues();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, newRadious);
    }

    private void Forget()
    {
        Collider[] ObjesctsInsideTheRadius = Physics.OverlapSphere(transform.position, newRadious, playerMask);

        if (ObjesctsInsideTheRadius.Length != 0)
        {
            Transform target = ObjesctsInsideTheRadius[0].transform;
            Vector3 targetDirection = (target.position - transform.position).normalized;

            if (Vector3.Angle(transform.forward, targetDirection) < newAngle / 2)
            {
                float distanceToTarget = Vector3.Distance(transform.position, target.position);

                if (!Physics.Raycast(transform.position, targetDirection, distanceToTarget, obstacleMask)) forget = 0;
                else forget += Time.deltaTime;
            }
            else forget += Time.deltaTime;
        }
        else forget += Time.deltaTime;     
    }
}
