using UnityEngine;

public class EnemyStates : MonoBehaviour
{
    //Referencia del editor
    [SerializeField] PatrolBehaviour patrol;
    [SerializeField] FollowBehaviour follow;
    [SerializeField] FieldOfView fov;

    public EnemyState eState;
    [SerializeField] public enum EnemyState
    {
        Patrolling,
        Follow,
        Combat,
        Dead
    }

    public void ChangeState(EnemyState state)
    {
        eState = state;

        DisableBehaviours();

        switch (eState)
        {
            case EnemyState.Patrolling:
                patrol.enabled = true;
                break;
            case EnemyState.Follow:
                follow.enabled = true;
                break;
            case EnemyState.Combat:
                break;
            case EnemyState.Dead:
                break;
            default:
                break;
        }
    }

    void DisableBehaviours()
    {
        patrol.enabled = false;
        follow.enabled = false;
    }
}
