using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckGroundedBehaviour : MonoBehaviour
{
    GameObject groundChecker;
    [SerializeField] LayerMask groundMask;
    [SerializeField] float groundDistance;

    void Start()
    {
        groundChecker = GameObject.Find("GroundChecker");
    }

    public bool isPlayerGrounded()
    {
        if (Physics.CheckSphere(groundChecker.transform.position, groundDistance, groundMask)) return true;
        else return false;
    }

    private void OnDrawGizmos()
    {
        var groundChecker = GameObject.Find("GroundChecker");
        Gizmos.DrawWireSphere(groundChecker.transform.position, groundDistance);
    }
}
