using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    PlayerStateController playerEvent;
    PlayerData playerData;

    public static GameManager Instance
    {
        get { return _instance; }
    }

    private void OnEnable()
    {
        playerEvent.onBeeingHit += CheckPlayerDeath;
    }

    private void OnDisable()
    {
        
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
        DontDestroyOnLoad(this.gameObject);

        playerEvent = GameObject.Find("Player").GetComponent<PlayerStateController>();
        playerData = playerEvent.playerData;
    }

    void CheckPlayerDeath(float val)
    {
        if (playerData.health >= 0) PlayerDeath();
    }
    public void PlayerDeath()
    {
        SceneManager.LoadScene("StartScene");
    }

    void Update()
    {
        
    }
}
