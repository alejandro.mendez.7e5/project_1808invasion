using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    
    [Header("Minimap and Sliders")]
    [SerializeField] Image healthBar;
    [SerializeField] Image staminaBar;

    [Header("Efecto de sangre")]
    [SerializeField] Image bloodEffectImage;
    public int bloodStartHealth;
    public int bloodMaxAlpha;
    public int bloodMinAlpha;

    [Header("Efecto de fundido a negro")]
    [SerializeField] Image blackFade;
    public int blackStartHealth;
    public int blackMaxAlpha;
    public int blackMinAlpha;

    [Header("Player Data")]
    public PlayerStateController playerEvents;
    [SerializeField] PlayerData playerData;

    private void OnEnable()
    {
        instance = this;
        //playerEvents = this.GetComponent<PlayerStateController>();
        GameObject.Find("Player").GetComponent<PlayerStateController>();
        playerEvents.onBeeingHit += updateHealthBar;
        playerEvents.onSprint += updateStaminaBar;
    }

    private void OnDisable()
    {
        playerEvents.onBeeingHit -= updateStaminaBar;
        playerEvents.onSprint -= updateHealthBar;
    }
    public void updateBloodEfect()
    {
        //1 Calcula el porcentaje de vida que le queda
        var currentHealthPorcentaje = (playerData.health * 100) / playerData.maxHealth;

        //2 Cuanto valor alpha vale cada 1% de vida
        var rangoAlpha = bloodMaxAlpha - bloodMinAlpha; // = 205
        float alphaIndividual = rangoAlpha / bloodStartHealth; // = 5.125

        //3 Calcula cuanta vida ha quitado
        var vidaQuitada = bloodStartHealth - currentHealthPorcentaje;

        //4 Calcula el alpha que le corresponde
        vidaQuitada *= alphaIndividual;
        vidaQuitada /= 100; //El alpha es sobre 100

        //5 Le asigna el color
        var color = bloodEffectImage.color;
        color.a = vidaQuitada;
        bloodEffectImage.color = color;
    }

    public void updateFadeEfect()
    {
        var currentHealthPorcentaje = (playerData.health * 100) / playerData.maxHealth;
        var rangoAlpha = blackMaxAlpha - blackMinAlpha; // 50
        float alphaIndividual = rangoAlpha / blackStartHealth; //3.333...
        var vidaQuitada = blackStartHealth - currentHealthPorcentaje;
        vidaQuitada *= alphaIndividual;
        vidaQuitada /= 100; 
        var color = blackFade.color;
        color.a = vidaQuitada;
        blackFade.color = color;
    }

    void updateHealthBar(float health)
    {
        float amount = (health / 100f) * 180f / 360;
        healthBar.fillAmount = amount;
    }

    void updateStaminaBar(float stamina)
    {
        float amount = (stamina / 50f) * 180f / 360;
        staminaBar.fillAmount = amount;
    }
}
