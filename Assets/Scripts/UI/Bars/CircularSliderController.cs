using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CircularSliderController : MonoBehaviour
{
    public Image fullBar;
    public float barValue;

    void Update()
    {
        updateBarValue();
    }

    void updateBarValue()
    {
       float amount = (barValue / 100f) * 180f / 360;
       fullBar.fillAmount = amount;
    }
}
