using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapIcon : MonoBehaviour
{

    [SerializeField] Transform follow;

    private void LateUpdate()
    {
        UpdatePosition();
    }

    void UpdatePosition()
    {
        var fixedPosition = new Vector3();
        
        fixedPosition.x = follow.position.x;
        fixedPosition.z = follow.position.z;
        fixedPosition.y = 10;

        this.transform.position = fixedPosition;
    }
}
