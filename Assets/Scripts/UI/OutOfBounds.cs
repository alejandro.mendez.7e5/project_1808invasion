using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutOfBounds : MonoBehaviour
{
    BlackScreenOut bS;
    Coroutine corrutineCD; //corrutina
    public float time; //tiempo maximo
    public float countDown; //tiempo restante

    private bool doTheFun;

    void Start()
    {
        bS = GameObject.Find("BlackScreenLoose").GetComponent<BlackScreenOut>();
        bS.gameObject.SetActive(false);
        doTheFun = false;
    }

    private void Update()
    {
        if (doTheFun) Fun();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            countDown = time;
            doTheFun = false;
            bS.gameObject.SetActive(false);

            //if(corrutineCD!=null) StopCoroutine(corrutineCD);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            doTheFun = true;
            Debug.LogWarning("Has salido de la zona");
            bS.gameObject.SetActive(true);
            
            //corrutineCD = StartCoroutine(outOfBoundsCountDown());
        }
    }

    private void Fun()
    {
        if (countDown >= 0) countDown -= Time.deltaTime;
        else
        {
            doTheFun = false;
            GameManager.Instance.PlayerDeath();
        }
    }



}