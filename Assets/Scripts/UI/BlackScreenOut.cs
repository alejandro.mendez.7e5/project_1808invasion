using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BlackScreenOut : MonoBehaviour
{
    TextMeshProUGUI seconds;
    OutOfBounds oB;

    void Start()
    {
        seconds = GameObject.Find("Timer").GetComponent<TextMeshProUGUI>();
        oB = GameObject.Find("LimitMap").GetComponent<OutOfBounds>();
    }

    void Update()
    {
        seconds.text = oB.countDown.ToString("F0"); //Para sin decimales
    }
}
