using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SettingsMenu : MonoBehaviour
{
    private GameObject _settingMenu;

    private bool _submenu;
    private bool _audiomenu;
    private GameObject _submenuGO;
    [SerializeField] private GameObject pauseFirstButton, optionsFirstButton, optionsClosedButton;


    public void Awake()
    {
        _settingMenu = GameObject.Find("SettingsMenu");
        _submenuGO = GameObject.Find("OptionsSubMenu");
        _submenuGO.SetActive(false);
        
        _submenu = false;
        _audiomenu = false;
        EventSystem.current.SetSelectedGameObject(pauseFirstButton);
    }
    
    
    public void ConfigurationSubMenuClicked()
    {
        _submenu = !_submenu;
        if (_submenu)
        {
            _submenuGO.SetActive(true);
            EventSystem.current.SetSelectedGameObject(optionsFirstButton);
        }

        else if (!_submenu)
        {
            _submenuGO.SetActive(false);
            EventSystem.current.SetSelectedGameObject(pauseFirstButton);
        }
    }

    public void ExitMenu()
    {
        _submenuGO.SetActive(false);
        _settingMenu.SetActive(false);
        Time.timeScale = 1f;
    }
}
