using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapCamController : MonoBehaviour
{
    [SerializeField] Transform player;

    private void LateUpdate()
    {
        Vector3 minimapCamPosition = player.position; //coge la posici�n del jugador
        minimapCamPosition.y = transform.position.y; //se mantiene en la y en la que ya esta (para no bajar a la altura del player)
        transform.position = minimapCamPosition; //se mueve a la posici�n del jugador

        // De esta manera se consigue que el mapa se mueva con el jugador, pero que este siempre se encuentre en el centro
        // tampoco rota
    }
}
