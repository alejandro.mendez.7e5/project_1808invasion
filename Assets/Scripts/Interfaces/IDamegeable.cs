using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamegeable 
{
    void ApplyDamage(float damageTaken);
}
