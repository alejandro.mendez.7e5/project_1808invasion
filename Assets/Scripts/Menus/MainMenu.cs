using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class MainMenu : MonoBehaviour
{
    private bool _submenu;
    private bool _audiomenu;
    private GameObject _submenuGO;
    [SerializeField] private GameObject pauseFirstButton,optionsFirstButton,optionsClosedButton;
    [SerializeField] private Animator animator; 

    public void Awake()
    {
        _submenuGO = GameObject.Find("OptionsSubMenu");
        _submenuGO.SetActive(false);
        _submenu = false;
        _audiomenu=false;
        EventSystem.current.SetSelectedGameObject(pauseFirstButton);
    }
    public void ConfigurationSubMenuClicked()
    {
        _submenu = !_submenu;
        if (_submenu)
        {
           _submenuGO.SetActive(true);
            EventSystem.current.SetSelectedGameObject(optionsFirstButton);
        }

        else if (!_submenu)
        {
           _submenuGO.SetActive(false);
            EventSystem.current.SetSelectedGameObject(optionsClosedButton);
        }
    }
    public void JumpToAnotherScene()
    {
        StartCoroutine(FadeIn());
    }
    public void ExitGame()
    {
        Application.Quit();
    }
    IEnumerator FadeIn()
    {

        animator.SetTrigger("Start");

        yield return new WaitForSeconds(1f);

        SceneManager.LoadScene("AlejandroScene");
    }
}
