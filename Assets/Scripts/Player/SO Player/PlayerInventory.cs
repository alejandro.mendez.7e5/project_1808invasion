using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Inventory_Data", menuName = "New ScriptableObject/New Inventory")]
public class PlayerInventory : ScriptableObject
{
    public List<Weapon> weapons;
    public List<Item> items;
}
