using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerData", menuName = "ScriptableObjects/Characters/PlayerData")]

public class PlayerData : CharacterSO
{
    public float maxHealth, health, maxStamina, stamina;

    private void OnEnable()
    {
        health = maxHealth;
        stamina = maxStamina;
    }
}
