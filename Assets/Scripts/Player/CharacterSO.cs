using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSO : ScriptableObject
{
    [SerializeField] protected float life;
    [SerializeField] protected float currentHealth;
    [SerializeField] protected float speed;
    [SerializeField] protected float money;

    public float Life { get { return life; } }
    public float CurrentHealth { get { return currentHealth; } set { currentHealth = value; } }
    public float Speed { get { return speed; } }
    public float Money { get { return money; } }

}
