using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class OpenMenu : MonoBehaviour
{
    private GameObject _settingMenu;
    PlayerInput playerInput;
    public bool OpenClosed;
    private void OnEnable()
    {   _settingMenu = GameObject.Find("SettingsMenu");
        _settingMenu.SetActive(false);
        OpenClosed = false;
        playerInput = GetComponent<PlayerInput>();
        playerInput.actions["Menu"].performed += ctx => openCloseMenu();
    }
    private void OnDisable()
    {
        playerInput.actions["Menu"].performed -= ctx => openCloseMenu();
    }

    private void openCloseMenu()
    {
        if (!OpenClosed) {
            Time.timeScale = 0f;
            OpenClosed = true;
           _settingMenu.SetActive(OpenClosed);
            EventSystem.current.SetSelectedGameObject(GameObject.Find("Options"));
            GameObject.Find("OptionsSubMenu").SetActive(false);
           
        }
        else
        {
            OpenClosed = false;
            _settingMenu.SetActive(OpenClosed);
            if (GameObject.Find("OptionsSubMenu")) GameObject.Find("OptionsSubMenu").SetActive(OpenClosed);
            Time.timeScale = 1f;
        }
    }
}
