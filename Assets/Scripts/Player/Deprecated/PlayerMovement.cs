using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;
    public float speed = 12f;
    public float gravity = -9.81f;

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    Vector3 velocity;
    enum state
    {
        ground,jump,fall
    }
    [SerializeField] private state jumpstate;
    // Start is called before the first frame update
    void Start()
    {
        jumpstate = state.ground;
    }

    // Update is called once per frame
    void Update()
    {
        if (Physics.CheckSphere(groundCheck.position, groundDistance, groundMask)) { jumpstate = state.ground; }
        else { jumpstate = state.fall; }
        if (jumpstate == state.ground && velocity.y < 0) { velocity.y = 0f; }
        else if(jumpstate == state.fall){
            velocity.y += gravity * Time.deltaTime;
        }
        
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        Vector3 move = transform.right * x + transform.forward * z + transform.up*velocity[1];

        controller.Move(move*speed*Time.deltaTime);
    }
}
