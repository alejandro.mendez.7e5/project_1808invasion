using UnityEngine;
using UnityEngine.InputSystem;



[RequireComponent(typeof(CharacterController))]
public class PlayerControler : MonoBehaviour
{
    private CharacterController controller; //"Rigidbody"
    private WeaponClass _weaponClass;// acciones de combate

    private PlayerInput _pInput;

    private Vector3 playerVelocity; //"x" y "z"



    [SerializeField] private PlayerInventory inventory;



    //Variables a poner en ScriptableObject

    [SerializeField] private float playerSpeed = 2.0f; //

    [SerializeField] private float jumpHeight = 1.0f; //

    [SerializeField] private float sprintSpeed = 1.75f; //

    [SerializeField] private float crouchSpeed = 0.75f; //
   
    private const float gravityValue = -9.81f;


    //Para agacharse
    private float normalHeight=2f;
    private float crouchHeight = 0.75f;



    //Para saltar

    [SerializeField] private GameObject GroundChecker;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    //bool grounded = false;

    private enum jumpStatus
    {
        ground,jumping,falling //solo se agarra en falling
    }
    public enum movementStatus
    {
        walk,run,crouch,swim,dive,hanging
    }

    movementStatus mStatus;
    jumpStatus jStatus;
    public Transform cameraTransform;
    
    private void Start()
    {
        Debug.Log("Primero va start");
        jStatus = jumpStatus.ground;
        mStatus = movementStatus.walk;
        controller = GetComponent<CharacterController>();
        cameraTransform = Camera.main.transform;
        _weaponClass = GetComponent<WeaponClass>();
        
    }

    private void OnEnable()

    {
        _pInput = GetComponent<PlayerInput>();
        _pInput.actions["Jump"].performed += ctx => Test();
        _pInput.actions["Crouch"].performed += ctx => Crouch();
    }

    void Test()

    {

        Debug.Log("Esto funciona. Yay!");

    }
    void Update()
    {
        //Attack();
        cameraRoation();
        jumpChecker();
        movement();
        // Changes the height position of the player..
     
        controller.Move(playerVelocity * Time.deltaTime); //Sin esto no cae, no tiene gravedad
    
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }

       // beforePlayerPosition = transform.position;
        }
  
    public movementStatus GetMovementStatus()
    {
        return mStatus;
    }
    private void movement()
    {
        if (jStatus==jumpStatus.ground && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }

        //Reads the keyboard/controler input  to move the character

        Vector2 movement = new Vector2(0f, 0f);

   
        // if (inputManager.PlayerCrouchedThisFrame()) crouchChecker();
        
        //Checks if the player is running
        if(mStatus!=movementStatus.crouch)runChecker(movement);

        switch (mStatus)
        {
            case movementStatus.run:
                movement.x *= sprintSpeed;
                movement.y *= sprintSpeed;
                break;

            case movementStatus.crouch:
                movement.x *= crouchSpeed;
                movement.y *=crouchSpeed;
                break;
        }
                         
        Vector3 move = new Vector3(movement.x, 0f, movement.y);

        move = cameraTransform.forward * move.z + cameraTransform.right * move.x;
        move.y = 0f;
        
        controller.Move(move * Time.deltaTime * playerSpeed); //Esto es el WASD
    }

    private void jumpChecker()
    {
        //Si no est� tocando el suelo, ni est� colgando entonces est� cayendo
        if (Physics.CheckSphere(GroundChecker.transform.position, groundDistance, groundMask)) { jStatus = jumpStatus.ground; }
        else if(mStatus != movementStatus.hanging) { jStatus = jumpStatus.falling; }
        
        //Si est� en el suelo y la velocidad vertical es menor que cero, velocidad vertical (y) = -2f
        if (jStatus == jumpStatus.ground && playerVelocity.y < 0) { playerVelocity.y = -2f; }
        else if (jStatus == jumpStatus.falling)
        {
            //Si est� cayendo, se le suma el valor de la gravedad * Time.deltaTime
            playerVelocity.y += gravityValue * Time.deltaTime;
        }
        
        //Si est� en el suelo y se pulsa saltar
        if (jStatus == jumpStatus.ground)
        {
            jStatus = jumpStatus.jumping;
            playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
        }
        //Si no si est� cayendo y no est� colgando mira si se puede colgar
        else if (jStatus == jumpStatus.falling && mStatus!= movementStatus.hanging)
        {
            climbCheck();
        }  

        //Siempre, independientemente de todo lo demas se le suma la velocidad de la gravedad
        playerVelocity.y += gravityValue * Time.deltaTime;
    }

    bool JumpCheck()

    {

        if (jStatus == jumpStatus.ground || mStatus == movementStatus.hanging) return true;

        else return false;

    }

    void Crouch()

    {

        if (mStatus != movementStatus.crouch && jStatus == jumpStatus.ground)

        {

            mStatus = movementStatus.crouch;

            controller.height = crouchHeight; //Buscar formula �? 60%

        }

        else if (mStatus == movementStatus.crouch && jStatus == jumpStatus.ground)

        {

            mStatus = movementStatus.walk;

            controller.height = normalHeight;

        }

    }

    //Para que el anim detecte bien //Al dejar de mover el stick llamar a evento que ponga el mStatus en walk
    private void runChecker(Vector2 movement)
    {
       if (movement.x == 0 && movement.y == 0)
        {
            mStatus = movementStatus.walk; 
        }
    }

    private void crouchChecker()
    {
        if (mStatus != movementStatus.crouch)
        {
            mStatus = movementStatus.crouch;
            //grounded = !grounded;
            controller.height = crouchHeight;
        }
        else if (mStatus == movementStatus.crouch)
        {
            mStatus = movementStatus.walk;
            //grounded = !grounded;
            controller.height = normalHeight;
        }
    }

    void climbCheck()
    {
        //variable feta per Carlos Carrasco
        var asd = transform.forward * 0.75f;

        Debug.DrawRay(asd+transform.position,Vector3.up,Color.green);
        Debug.DrawRay(transform.position, transform.forward, Color.red);
    
        var verticalRaycast = new Ray(asd + transform.position, Vector3.up);
     
        RaycastHit hit;
        if (Physics.Raycast(verticalRaycast, out hit) && jStatus == jumpStatus.falling)
        {
            var horizontalRaycast = new Ray(transform.position, transform.forward);


            if (Physics.Raycast(horizontalRaycast, out hit))
            {
                if (hit.collider.tag == "Platform")
                {
                    transform.position = hit.point;
                    jStatus = jumpStatus.ground;
                    mStatus = movementStatus.hanging;

                }
            }
        }
    }
    
    void cameraRoation()
    {
        this.transform.rotation = cameraTransform.rotation;
    }
}

