
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableFall", menuName = "ScriptableObjects/ScriptableAction/ScriptableFall")]
public class ScriptableFall : ScriptableAction
{
    CharacterController cController;
    CheckGroundedBehaviour cGrounded;

    Transform transform;
    PlayerStateController playerStateController; //Específico del player

    public override void OnFinishedState()
    {

    }

    public override void OnSetState(StateController sC)
    {
        base.OnSetState(sC);
        cController = sC.GetComponent<CharacterController>();
        cGrounded = sC.GetComponent<CheckGroundedBehaviour>();
        transform = sc.gameObject.transform;
        playerStateController = (PlayerStateController)sc;
    }

    public override void OnUpdate()
    {
        ApplyFallingExtraGravity();
        CheckHangUp();

        if (cGrounded.isPlayerGrounded()) sc.StateTransition(playerStateController.idle);     
    }

    private void ApplyFallingExtraGravity()
    {
        Vector2 Vgravity = new Vector2(0, 0);
        float gravity = -9.81f;

        Vgravity.y = gravity;
        cController.Move(Vgravity * Time.deltaTime);
    }

    private void CheckHangUp()
    {
        var rayDirection = transform.forward * 0.75f;

        Debug.DrawRay(rayDirection + transform.position, Vector3.up, Color.green);
        Debug.DrawRay(transform.position, transform.forward, Color.red);

        var verticalRaycast = new Ray(rayDirection + transform.position, Vector3.up);

        RaycastHit hit;

        if (Physics.Raycast(verticalRaycast, out hit))
        {
            var horizontalRaycast = new Ray(transform.position, transform.forward * 0.75f); //El 0.75 es un triple

            if (Physics.Raycast(horizontalRaycast, out hit))
            {
                if (hit.collider.tag == "Platform") sc.StateTransition(playerStateController.hang);   
            }
        }
    }
}

