using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

//Clase base. Por cada acci�n, el c�digo
public abstract class ScriptableAction : ScriptableObject
{
    protected StateController sc;
    protected Animator animator;
    protected PlayerInput playerInput;

    public virtual void OnFinishedState() { } //Al terminar / salir del estado
    
    //Al entrar en el estado
    public virtual void OnSetState(StateController sC) {
        sc = sC;
        animator = sc.GetComponent<Animator>();
        playerInput = sc.GetComponent<PlayerInput>();
    }

    public virtual void OnUpdate() {
        sc.currentState.action.OnUpdate();
    } //Durante / mientr�s est� en el estado
}
