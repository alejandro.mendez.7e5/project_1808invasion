using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableIdle", menuName = "ScriptableObjects/ScriptableAction/ScriptableIdle")]
public class ScriptableIdle : ScriptableAction
{ 
    public override void OnFinishedState()
    { }

    public override void OnSetState(StateController sC)
    {
        Debug.Log("Entro en Idle");
        base.OnSetState(sC);
        sC.GetComponent<PlayerAnimationController>().AnimationIdle(0.0f);
    }

    public override void OnUpdate()
    { }
}
