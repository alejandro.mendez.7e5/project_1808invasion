
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[CreateAssetMenu(fileName = "ScriptableJump", menuName = "ScriptableObjects/ScriptableAction/ScriptableJump")]
public class ScriptableJump : ScriptableAction
{
    PlayerStateController player;

    public override void OnSetState(StateController sC)
    {
        base.OnSetState(sC);   
        player = (PlayerStateController)sc;
        player.jumpB.enabled = true;
    }

    public override void OnFinishedState()
    {
        player.jumpB.enabled = false;
    }

    public override void OnUpdate()
    {
        if (StartFalling()) sc.StateTransition(player.fall);
    }

    public bool StartFalling()
    {
        return player.jumpB.isOnTop;
    }
}
