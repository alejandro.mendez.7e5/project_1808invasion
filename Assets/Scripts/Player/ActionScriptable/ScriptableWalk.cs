
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableWalk", menuName = "ScriptableObjects/ScriptableAction/ScriptableWalk")]
public class ScriptableWalk : ScriptableAction
{
    Transform camTransform;
    CharacterController controller;

    Vector2 inputDirection;
    Vector3 moveTowards;

    public override void OnFinishedState()
    {
        Debug.LogWarning("Salgo de Walking");
    }

    public override void OnSetState(StateController sC)
    {
        Debug.LogWarning("Entro en Walking");
        
        base.OnSetState(sC);
        camTransform = GameObject.Find("Main Camera").transform;
        controller = sc.GetComponent<CharacterController>();
        sC.GetComponent<PlayerAnimationController>().AnimationWalk(0.5f);
    }

    public override void OnUpdate()
    {
        inputDirection = playerInput.actions["Movement"].ReadValue<Vector2>();

        moveTowards = new Vector3(inputDirection.x, 0f, inputDirection.y);
        // 33
        moveTowards = ((camTransform.forward * moveTowards.z) + (camTransform.right * moveTowards.x)) * Time.deltaTime * 5; //El 5 se cambia por el speed del ScriptableObject
        moveTowards.y = 0;
        controller.Move(moveTowards);
    }

}