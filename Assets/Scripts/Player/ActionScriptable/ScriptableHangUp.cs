using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableHangUp", menuName = "ScriptableObjects/ScriptableAction/ScriptableHangUp")]
public class ScriptableHangUp : ScriptableAction
{
    public override void OnFinishedState()
    {
        
    }

    public override void OnSetState(StateController sC)
    {
        base.OnSetState(sC);
    }

    public override void OnUpdate()
    {

    }
}
