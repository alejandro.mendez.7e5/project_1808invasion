using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerAnimationController : MonoBehaviour
{
    private Animator _animator;
    public float _transition;


    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    public void AnimationIdle(float transition)
    {
        _animator.SetFloat("Speed", transition);
    }

    public void AnimationWalk(float transition)
    {
        _animator.SetFloat("Speed", transition);
    }

    private void AnimationRun(float transition)
    {
        _animator.SetFloat("Speed", transition);
    }

    private void AnimationCrouch()
    {

    }

    private void AnimationJump()
    {

    }
    public void AnimationAttack(int click)
    {
        _animator.SetInteger("Attack", click);
    }

    public void AnimationAttack1(int click)
    {
        _animator.SetInteger("Attack", click);
    }

    public void AnimationAttack2(int click)
    {
        _animator.SetInteger("Attack", click);
    }

    public void AnimationAttack3(int click)
    {
        _animator.SetInteger("Attack", click);
    }

    public void Aiming(bool aim)
    {
        _animator.SetBool("isAiming", aim);
    }
    public void AnimationShoot()
    {
        _animator.SetTrigger("Shoot");
    }

    private void ControllerTransition(float maxTransition)
    {
        
        if(_transition < maxTransition)
        {
            _transition += 1f * Time.deltaTime;
        }
        else if(_transition > maxTransition )
        {
            _transition -= 1f * Time.deltaTime;
        }
    }
}
