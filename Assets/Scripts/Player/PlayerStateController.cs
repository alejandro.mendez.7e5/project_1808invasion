using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerStateController : StateController
{
    //Eventos, no valores
    public delegate void PlayerLife(float damage);
    public delegate void PlayerStamina(float stamina);

    public event PlayerLife onBeeingHit;
    public event PlayerStamina onSprint;

    //Los datos del jugador (vida, resistencia...)
    public PlayerData playerData;
    public PlayerInventory inventory;

    //The mapped player input
    PlayerInput playerInput;

    private Weapon currentWeapon;

    //All the states de player can perform
    [SerializeField] public ScriptableState idle, walk, hang, jump, fall;

    //For aplying gravity
    CharacterController cController;

    //An Vector2, used for compare and check if a Vector2 is empty
    Vector2 V2zero;

    //Check if is Crouched. Crouching you do the almost the same, but move slower and the hitbox is smaller too
    bool isCrouching;

    //Aiming and blocking are layer masks too
    bool isAiming;
    bool isBlocking;

    //Check if the inventory is open
    bool isInventoryOpen;

    CheckGroundedBehaviour cGrounded;
    public JumpBehaviour jumpB;

    [SerializeField] Transform mainCamera;

    //Adds all the input events 
    private void OnEnable()
    {
        playerInput = GetComponent<PlayerInput>();

        playerInput.actions["Jump"].performed += ctx => Jump(); //SOAction --> Fall / Hang --> Idle
        playerInput.actions["Crouch"].performed += ctx => Crouch(); //LayerMask
        playerInput.actions["BlockAim"].performed += ctx => AimOrBlock(); //LayerMask
        playerInput.actions["Interact"].performed += ctx => Interact(); //Funcion
        playerInput.actions["AttackShoot"].performed += ctx => AttackOrShoot(); //Funci�n
        playerInput.actions["Reload"].performed += ctx => Reload(); //Funci�n
        playerInput.actions["Inventory"].performed += ctx => Inventory(); //Funci�n
        playerInput.actions["Weapon1"].performed += ctx => SelectWeapon1(); //Funcion
        playerInput.actions["Weapon2"].performed += ctx => SelectWeapon2(); //Funcion
        playerInput.actions["Weapon3"].performed += ctx => SelectWeapon3(); //Funcion

        if (jumpB == null) jumpB = GetComponent<JumpBehaviour>();
    }

    //Removes all the input events
    private void OnDisable()
    {
        playerInput.actions["Jump"].performed -= ctx => Jump(); //SOAction --> Fall / Hang --> Idle
        playerInput.actions["Crouch"].performed -= ctx => Crouch(); //LayerMask
        playerInput.actions["BlockAim"].performed -= ctx => AimOrBlock(); //LayerMask
        playerInput.actions["Interact"].performed -= ctx => Interact(); //Funcion
        playerInput.actions["AttackShoot"].performed -= ctx => AttackOrShoot(); //Funci�n
        playerInput.actions["Reload"].performed -= ctx => Reload(); //Funci�n
        playerInput.actions["Inventory"].performed -= ctx => Inventory(); //Funci�n
        playerInput.actions["Weapon1"].performed -= ctx => SelectWeapon1(); //Funcion
        playerInput.actions["Weapon2"].performed -= ctx => SelectWeapon2(); //Funcion
        playerInput.actions["Weapon3"].performed -= ctx => SelectWeapon3(); //Funcion
    }

    private void Start()
    {
        V2zero = new Vector2(0f, 0f);
        cController = GetComponent<CharacterController>();
        cGrounded = GetComponent<CheckGroundedBehaviour>();
        mainCamera = GameObject.Find("Main Camera").transform;
        DefaultWeapon();
    }

    private void OnTriggerEnter(Collider other)
    {
        //cositas de recibir el ataque
    }

    private void Update()
    {
        //ApplyDamage
        if (Input.GetKeyDown(KeyCode.M))
        {
            playerData.health -= 1; //Damage
            if (onBeeingHit != null) onBeeingHit(playerData.health);

            if (playerData.health <= playerData.maxHealth * 40 / 100)
            {
                //Si la vida llega a menos del 40% del total
                UIManager.instance.updateBloodEfect();
            }

            if (playerData.health <= playerData.maxHealth * 15 / 100)
            {
                //Si la vida llega a menos del 15%
                UIManager.instance.updateFadeEfect();
            }
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            playerData.stamina -= Time.deltaTime * 5;
            if (onSprint != null) onSprint(playerData.stamina);
        }
        else
        {
            if (playerData.stamina <= 50)
            {
                playerData.stamina += Time.deltaTime;
                if (onSprint != null) onSprint(playerData.stamina);
            }
        }

        RotatePlayer();

        currentState.action.OnUpdate();

        if (!cGrounded.isPlayerGrounded() && currentState != jump) StateTransition(fall);
        if (currentState != hang) ApplyGravity();

        //Movement tiene que estar en Update, porque cada frame mira si se mueve o no
        //Lo dem�s como es pulsar un bot�n se puede poner mejor por evento

        //Si te est�s moviendo
        if (playerInput.actions["Movement"].ReadValue<Vector2>() != V2zero)
        {
            //Si no est�s ya en caminar te pone en caminar
            if (currentState != walk) StateTransition(walk);
        }
        //Si no te est�s moviendo, si no est�s cayendo es que estas quieto
        else if (currentState != fall) StateTransition(idle);
    }

    private void ApplyGravity()
    {      
        Vector2 Vgravity = new Vector2(0, 0);
        float gravity = -9.81f;

        Vgravity.y = gravity;
        cController.Move(Vgravity * Time.deltaTime);       
    }

    void RotatePlayer()
    {
        var r = mainCamera.transform.rotation.eulerAngles;
        r.x = 0;
        this.gameObject.transform.rotation = Quaternion.Euler(r);
        this.gameObject.transform.localRotation = Quaternion.Euler(r);
    }

    private void Interact()
    {
        //Aqu� ya se monta la movida que haga falta, seg�n lo que sea y c�mo se interact�e.
        Debug.Log("Interact�o");
    }

    private void AttackOrShoot()
    {
        //If tengo arma blanca
        MeleeAttack();

        //If tengo un arma de fuego
        if (isAiming) Shoot();
    }

    void Shoot()
    {
        currentWeapon.ShootAttack();
    }

    void MeleeAttack()
    {
        currentWeapon.Attack();
    }

    private void Reload()
    {
        Debug.Log("Recargo");
    }

    private void SelectWeapon1()
    {
        currentWeapon.DestroyInstance();
        currentWeapon = inventory.weapons[0];
        currentWeapon.Instantiate(currentWeapon);
    }

    private void SelectWeapon2()
    {
        currentWeapon.DestroyInstance();
        currentWeapon = inventory.weapons[1];
        currentWeapon.Instantiate(currentWeapon);
    }
    private void SelectWeapon3()
    {
        currentWeapon.DestroyInstance();
        currentWeapon = inventory.weapons[2];
        currentWeapon.Instantiate(currentWeapon);
    }

    private void DefaultWeapon()
    {
        currentWeapon = inventory.weapons[0];
        currentWeapon.Instantiate(currentWeapon);
    }

    private void Inventory()
    {
        if (isInventoryOpen)
        {
            CloseInventory();
        }
        else
        {
            OpenInventory();
        }

        isInventoryOpen = !isInventoryOpen;
    }

    private void OpenInventory()
    {
        Debug.Log("Abro el inventario");
    }

    private void CloseInventory()
    {
        Debug.Log("Cierro el inventario");
    }

    private void Crouch()
    {
        //Agacharse ya no es un estado, es una m�scara de animaci�n. De esta manera se evitan problemas entre estados.
        //Con la m�scara se consigue que haga b�sicamente lo mismo de cintura para arriba, y de cintura para abajo tiene las piernas flexionadas.

        if (isCrouching)
        {
            Debug.Log("Me levanto");
        }
        else
        {
            Debug.Log("Me agacho");
        }

        isCrouching = !isCrouching;
    }

    private void AimOrBlock()
    {
        //If tiene un escudo
        Block();

        //If tiene un arma de fuego
        Aim();
    }

    private void Block()
    {
        if (isBlocking)
        {
            Debug.Log("Dejo de bloquear");
        }
        else
        {
            Debug.Log("Bloqueo");
        }

        isBlocking = !isBlocking;
    }

    private void Aim()
    {
        if (currentWeapon.Weapontype.ToString() == "ranged")
        {
            isAiming = true;
            currentWeapon.AimAttack(isAiming);
        }
        else
        {
            isAiming = false;
            currentWeapon.AimAttack(isAiming);
        }
    }

    
    private void Jump()
    {
        if (cGrounded.isPlayerGrounded()) StateTransition(jump);
        else Debug.Log("No puedo saltar en el aire!");
    }

}