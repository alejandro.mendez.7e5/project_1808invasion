using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpBehaviour : MonoBehaviour
{
    public float jumpHeight, jumpVelocity;
    public bool isOnTop;
    float objectiveAltura;
    CharacterController controller;

    private void OnEnable()
    {
        objectiveAltura = this.gameObject.transform.position.y + jumpHeight;
        controller = GetComponent<CharacterController>();
        isOnTop = false;
    }

    public void Update()
    {
        if (!isOnTop)
        {
            if (transform.position.y <= objectiveAltura) controller.Move(new Vector3(0f, jumpVelocity, 0f));
            else isOnTop = true;   
        }     
    }
}
