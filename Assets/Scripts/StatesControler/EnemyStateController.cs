using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStateController : StateController
{
    [Header("Scriptable States")]
    [SerializeField] public ScriptableState patrol;
    [SerializeField] public ScriptableState watch;
    [SerializeField] public ScriptableState follow;
    [SerializeField] public ScriptableState attack;
    [SerializeField] public ScriptableState die;

    [Header("Behaviours")]
    [SerializeField] PatrolBehaviour patrolB;
    [SerializeField] FollowBehaviour followB;
    [SerializeField] FieldOfView fovB;

    // Start is called before the first frame update
    void Start()
    {
        StateTransition(patrol);
        currentState = patrol;
    }

    private void Update()
    {
        currentState.action.OnUpdate();

        if (fovB.canSeePlayer) StateTransition(follow);
    }
}
