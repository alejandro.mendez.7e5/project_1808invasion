using UnityEngine;

public class StateController : MonoBehaviour
{
    public ScriptableState currentState;

    private void Update()
    {
        currentState.action.OnUpdate();
    }

    public void StateTransition(ScriptableState state)
    {
        if (currentState.stateTransitions.Contains(state))
        {
            currentState.action.OnFinishedState();
            currentState = state;
            currentState.action.OnSetState(this);
        }
    }
}
