using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableState", menuName = "ScriptableObjects/ScriptableState")]
public class ScriptableState : ScriptableObject
{
    public ScriptableAction action;
    public List<ScriptableState> stateTransitions;
}
